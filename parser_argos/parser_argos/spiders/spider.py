# coding: utf8
import scrapy
import re
import json
import csv
import sys
import logging
from parser_argos.items import ParserArgosItem

class ParserArgosSpider(scrapy.Spider):

    name = "argos"

    def start_requests(self):
        url = 'http://www.argos.co.uk/webapp/wcs/stores/servlet/ArgosAToZCategoriesView?langId=110&storeId=10151'
        yield scrapy.Request(url=url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.xpath(".//dd/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_sub_sub_category)

    # def parse_sub_category(self, response):
    #     urls = response.xpath(".//ul[@id='categoryList']//a/@href").extract()
    #     for url in urls:
    #         yield scrapy.Request(url=response.urljoin(url), callback=self.parse_sub_sub_category)

    def parse_sub_sub_category(self, response):
        product = response.xpath(".//div[@data-el='product-card']//a/@href").extract()
        for url in product:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)
        pagination = response.xpath("(.//a[@class='pagination__link']/@href)[last()]").extract_first(default=None)
        if pagination is not None:
            yield scrapy.Request(url=response.urljoin(pagination), callback=self.parse_sub_sub_category)




    def parse_set_item(self, response):
        urls = response.xpath(".//div[@data-el='product-card']//a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)


    def parse_item(self, response):
        product = ParserArgosItem()
        product['url'] = response.url
        product['id'] = response.xpath(".//small[@class='product-name-catNumber']/text()").extract_first(default='').replace('/','')
        product['img'] = response.xpath(".//div[@class='media-player']//img/@src").extract_first(default='')
        product['produckt_group'] = response.xpath(".//a[@class='breadcrumb__link']/span/text()").extract_first(
            default='')
        product['name'] = response.xpath(".//h1/span[@itemprop='name']/text()").extract_first(default='').replace('"','&#34;').replace(',','&#44;').strip()
        try:
            price = response.xpath(
                ".//li[@itemprop='price']/@content").extract_first().split("-")[
                0]
            product['price'] = float(price.replace('"','&#34;').replace(',','&#44;').replace(u'€','').replace(u',',''))
            if product['id'] != '':
                yield product
        except:
            logging.error("Price error in url: " + product['url'])



















